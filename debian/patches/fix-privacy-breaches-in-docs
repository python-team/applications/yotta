Description: Fix privacy breaches in documentation by using system JS libs
 We replace the bundled upstream bootstrap/jquery JS dists with links to minified
 versions provided by Debian packages
 .
 We also remove a Google tracker and link to a remote logo
Author: Nick Morrott <nickm@debian.org>
Forwarded: not-needed
Last-Update: 2025-02-08
---
--- a/docs/_layouts/default.html
+++ b/docs/_layouts/default.html
@@ -7,19 +7,9 @@
     <meta name="description" content="">
     <meta name="author" content="">
 
-    <link href='https://fonts.googleapis.com/css?family=Lato:100,400' rel='stylesheet' type='text/css'>
-    <link href="//yottabuild.org/style/css/style.css" rel="stylesheet" type="text/css">
-    <link href="{{ site.github.url }}/bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
-    <link href="{{ site.github.url }}/assets/css/docs.css" rel="stylesheet">
-    <!--[if lt IE 9]>
-      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
-      </script>
-    <![endif]-->
-    <link rel="shortcut icon" href="{{ site.github.url }}/assets/img/yt.png">
-    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ site.github.url }}/assets/ico/apple-touch-icon-144-precomposed.png">
-    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ site.github.url }}/assets/ico/apple-touch-icon-114-precomposed.png">
-    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ site.github.url }}/assets/ico/apple-touch-icon-72-precomposed.png">
-    <link rel="apple-touch-icon-precomposed" href="{{ site.github.url }}/assets/ico/apple-touch-icon-57-precomposed.png">
+    <link href="file:///usr/share/javascript/bootstrap5/css/bootstrap.css" rel="stylesheet">
+    <link href={{ "assets/css/docs.css" | absolute_url }} rel="stylesheet">
+    <link rel="shortcut icon" href={{ "assets/img/yt.png" | absolute_url }}>
     <style>
     </style>
   </head>
@@ -30,30 +20,30 @@
         <div class="col-sm-3">
           <ul id="menu" class="nav nav-list">
             <li class="nav-header">Using yotta</li>
-            <li data-section="yotta/introduction"><a href="{{ site.github.url }}/#introduction">Getting Started</a></li>
-            <li data-section="yotta/installing"><a href="{{ site.github.url }}/#installing">Installing</a></li>
-            <li data-section="yotta/cheatsheet"><a href="{{ site.github.url }}/reference/cheatsheet.html">Cheatsheet</a></li>
+            <li data-section="yotta/introduction"><a href={{ "index.html#introduction" | absolute_url }}>Getting Started</a></li>
+            <li data-section="yotta/installing"><a href={{ "index.html#installing" | absolute_url }}>Installing</a></li>
+            <li data-section="yotta/cheatsheet"><a href={{ "reference/cheatsheet.html" | absolute_url }}>Cheatsheet</a></li>
             <li class="nav-header">Tutorials</li>
-            <li data-section="tutorial/building"><a href="{{ site.github.url }}/tutorial/building.html">Building Modules</a></li>
-            <li data-section="tutorial/module"><a href="{{ site.github.url }}/tutorial/tutorial.html">Creating a Module</a></li>
-            <li data-section="tutorial/executable"><a href="{{ site.github.url }}/tutorial/tutorial.html#Creating%20an%20Executable">Creating an Executable</a></li>
-            <li data-section="tutorial/privaterepos"><a href="{{ site.github.url }}/tutorial/privaterepos.html">Private Repositories</a></li>
-            <li data-section="tutorial/targets"><a href="{{ site.github.url }}/tutorial/targets.html">Using Targets</a></li>
-            <li data-section="tutorial/testing"><a href="{{ site.github.url }}/tutorial/testing.html">Testing with yotta</a></li>
-            <li data-section="tutorial/yotta_link"><a href="{{ site.github.url }}/tutorial/yotta_link.html">Fixing a Bug in a Dependency</a></li>
+            <li data-section="tutorial/building"><a href={{ "tutorial/building.html" | absolute_url }}>Building Modules</a></li>
+            <li data-section="tutorial/module"><a href={{ "tutorial/tutorial.html" | absolute_url }}>Creating a Module</a></li>
+            <li data-section="tutorial/executable"><a href={{ "tutorial/tutorial.html#Creating%20an%20Executable" | absolute_url }}>Creating an Executable</a></li>
+            <li data-section="tutorial/privaterepos"><a href={{ "tutorial/privaterepos.html" | absolute_url }}>Private Repositories</a></li>
+            <li data-section="tutorial/targets"><a href={{ "tutorial/targets.html" | absolute_url }}>Using Targets</a></li>
+            <li data-section="tutorial/testing"><a href={{ "tutorial/testing.html" | absolute_url }}>Testing with yotta</a></li>
+            <li data-section="tutorial/yotta_link"><a href={{ "tutorial/yotta_link.html" | absolute_url }}>Fixing a Bug in a Dependency</a></li>
             <li class="nav-header">Best Practices</li>
-            <li data-section="tutorial/reuse"><a href="{{ site.github.url }}/tutorial/reuse.html">Writing Re-usable Software</a></li>
-            <li data-section="tutorial/releasing"><a href="{{ site.github.url }}/tutorial/release.html">Releasing Software with yotta</a></li>
-            <li data-section="tutorial/circulardeps"><a href="{{ site.github.url }}/tutorial/circulardeps.html">Handling Circular Dependencies</a></li>
+            <li data-section="tutorial/reuse"><a href={{ "tutorial/reuse.html" | absolute_url }}>Writing Re-usable Software</a></li>
+            <li data-section="tutorial/releasing"><a href={{ "tutorial/release.html" | absolute_url }}>Releasing Software with yotta</a></li>
+            <li data-section="tutorial/circulardeps"><a href={{ "tutorial/circulardeps.html" | absolute_url }}>Handling Circular Dependencies</a></li>
             <li class="nav-header">Reference</li>
-            <li data-section="reference/commands"><a href="{{ site.github.url }}/reference/commands.html">Command Reference</a></li>
-            <li data-section="reference/module"><a href="{{ site.github.url }}/reference/module.html">module.json</a></li>
-            <li data-section="reference/target"><a href="{{ site.github.url }}/reference/target.html">target.json</a></li>
-            <li data-section="reference/ignore"><a href="{{ site.github.url }}/reference/ignore.html">.yotta_ignore</a></li>
-            <li data-section="reference/config"><a href="{{ site.github.url }}/reference/config.html">Config System</a></li>
-            <li data-section="reference/buildsystem"><a href="{{ site.github.url }}/reference/buildsystem.html">Build System</a></li>
-            <li data-section="reference/registry"><a href="{{ site.github.url }}/reference/registry.html">yotta Public Registry</a></li>
-            <li data-section="reference/licenses"><a href="{{ site.github.url }}/reference/licenses.html">yotta Module Licenses</a></li>
+            <li data-section="reference/commands"><a href={{ "reference/commands.html" | absolute_url }}>Command Reference</a></li>
+            <li data-section="reference/module"><a href={{ "reference/module.html" | absolute_url }}>module.json</a></li>
+            <li data-section="reference/target"><a href={{ "reference/target.html" | absolute_url }}>target.json</a></li>
+            <li data-section="reference/ignore"><a href={{ "reference/ignore.html" | absolute_url }}>.yotta_ignore</a></li>
+            <li data-section="reference/config"><a href={{ "reference/config.html" | absolute_url }}>Config System</a></li>
+            <li data-section="reference/buildsystem"><a href={{ "reference/buildsystem.html" | absolute_url }}>Build System</a></li>
+            <li data-section="reference/registry"><a href={{ "reference/registry.html" | absolute_url }}>yotta Public Registry</a></li>
+            <li data-section="reference/licenses"><a href={{ "reference/licenses.html" | absolute_url }}>yotta Module Licenses</a></li>
           </ul>
         </div>
         <div class="col-sm-9 docs-content">
@@ -65,9 +55,7 @@
         <div class="row">
             <div class="footer">
                 <div class="mbed-logo">
-                    <a class="mbedLink" href="http://www.mbed.com" title="back to mbed home">
-                        <img src="http://yottabuild.org/images/ARMmbedLogo.svg">
-                    </a>
+                    <a class="mbedLink" href="http://www.mbed.com" title="back to mbed home">Back to mbed home</a>
                 </div>
                 <div class="copyright-bar">
                     <div class="copyright">© ARM Ltd. Copyright 2015 – ARM mbed IoT Device Platform</div>
@@ -84,24 +72,14 @@
     </footer>
 
     <style>
-      
+
     </style>
-    <script src="{{ site.github.url }}/bower_components/jquery/dist/jquery.min.js"></script>
-    <script src="{{ site.github.url }}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
+    <script src="file:///usr/share/javascript/jquery/jquery.min.js"></script>
+    <script src="file:///usr/share/javascript/bootstrap5/js/bootstrap.min.js"></script>
 
     <script>
       $('#menu [data-section="{{page.section}}"]').addClass('active');
     </script>
-
-    <script type="text/javascript">
-      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
-      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
-      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
-      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
-
-      ga('create', 'UA-55282605-1', 'auto');
-      ga('send', 'pageview');
-    </script>
   </body>
 </html>
 
